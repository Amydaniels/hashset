﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hashset
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            //Uses hashset to generate a set of random unique numbers
            Console.WriteLine("************************");
            Console.WriteLine("Hashset random generated");


            HashSet<int> hash = new HashSet<int>();

            Stopwatch sw2 = new Stopwatch();
            sw2.Start();

            for (int i = 0; i < 1000; i++)
            {
                hash.Add(rand.Next(10000, 99999999));
            }

            foreach (int num in hash)
            {
                Console.Write(" " + num);
            }
            Console.WriteLine("");

            sw2.Stop();
            Console.WriteLine("Hashset used: " + sw2.Elapsed);
            Console.ReadLine();


        }
    }
}
